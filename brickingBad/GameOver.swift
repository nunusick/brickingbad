//
//  gameOver.swift
//  brickingBad
//
//  Created by Nunusick on 12/5/18.
//  Copyright © 2018 s. All rights reserved.
//

import SpriteKit

class GameOver: SKScene {

    lazy var gameVC: GameViewController? = nil
    var label = SKLabelNode(fontNamed: "Avenir-Black")
    var backGround = SKSpriteNode(color: UIColor.clear, size: CGSize(width: 0, height: 0))
    
    func redrawLabel() {
        label.removeFromParent()
        label = SKLabelNode(fontNamed: "Avenir-Black")
        label.fontSize = 50
        label.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        label.text = (won ? "You won!!!" : "Looooser!)")
        self.addChild(label)
    }
    
    func setCanvas() {
        backGround.removeFromParent()
        backGround = SKSpriteNode(color: UIColor.clear, size: CGSize(width: self.frame.width, height: self.frame.height))
        backGround.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        backGround.texture = SKTexture(imageNamed: won ? "background" : "wood")
        self.addChild(backGround)
    }
    
    var won: Bool {
        didSet {
            setCanvas()
            redrawLabel()
        }
    }
    
    init(size: CGSize, won: Bool) {
        self.won = won
        super.init(size: size)
        setCanvas()
        redrawLabel()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        gameVC!.showBoard(redraw: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        won = false
        super.init(coder: aDecoder)
    }
}
