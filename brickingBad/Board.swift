//
//  GameScene.swift
//  brickingBad
//
//  Created by Nunusick on 12/2/18.
//  Copyright © 2018 s. All rights reserved.
//

import SpriteKit
import AVFoundation

class Board: SKScene, SKPhysicsContactDelegate {
    let mUrl = Bundle.main.url(forResource: "backg", withExtension: "mp3")
    
    enum SubjectCategory: UInt32 {
        case ball   = 0x01
        case brick  = 0x02
        case scate  = 0x04
        case hollow = 0x08
        case sButton = 0x10
        case pButton = 0x12
        
        func name() -> String {
            switch self {
            case .ball      : return "ball"
            case .brick     : return "brick"
            case .scate     : return "scate"
            case .hollow    : return "hollow"
            case .sButton   : return "sButton"
            case .pButton   : return "pButton"
            }
        }
        
        func categoryBit() -> UInt32 {
            return self.rawValue
        }
    }
    
    static let ballCategory = SubjectCategory.ball
    static let scateCategory = SubjectCategory.scate
    static let hollowCategory = SubjectCategory.hollow
    static let brickCategory = SubjectCategory.brick
    static let sButtonCategory = SubjectCategory.sButton
    static let pButtonCategory = SubjectCategory.pButton
    
    var musPlayer: AVAudioPlayer?

    var spaceH: Float {
        didSet {
            if spaceH > 20.0 {
                spaceH = 20.0
            } else if spaceH <= 0 {
                spaceH = 1
            }
        }
    }
    
    var spaceV: Float {
        didSet {
            if spaceV > 20.0 {
                spaceV = 20.0
            } else if spaceV <= 0 {
                spaceV = 1
            }
        }
    }
    
    var rowsCount: Int {
        didSet {
            if rowsCount > 7 {
                rowsCount = 7
            } else if rowsCount <= 0 {
                rowsCount = 1
            }
        }
    }
    
    var bricksInRow: Int {
        didSet {
            if bricksInRow > 40 {
                bricksInRow = 40
            } else if bricksInRow <= 0 {
                bricksInRow = 1
            }
        }
    }
    
    var offsetX: Float {
        didSet {
            if offsetX >= Float(self.frame.width / 2) {
                offsetX = Float(self.frame.width / 2 - 100.0)
            } else if offsetX <= 0 {
                offsetX = 1.0
            }
        }
    }
    
    var offsetY: Float {
        didSet {
            if offsetY >= Float(self.frame.height / 2) {
                offsetY = Float(self.frame.height / 2 - 100.0)
            } else if offsetY <= 0 {
                offsetY = 1.0
            }
        }
    }
    
    var brickWidth: Float {
        get {
            return Float(Float(self.frame.width) - Float(offsetX * 2)) / Float(bricksInRow) - spaceH
        }
    }
    
    var brickHeight: Float = 20.0
    
    func playBGMusic() {

        if musPlayer != nil && !musPlayer!.isPlaying {
            musPlayer!.play()
            return
        }

        if mUrl != nil {
            do {
                try musPlayer = AVAudioPlayer(contentsOf: mUrl!)
            } catch let error {
                print("Error loading sound file \(error.localizedDescription)")
                return
            }
            musPlayer!.numberOfLoops = -1
            musPlayer!.prepareToPlay()
            musPlayer!.play()
        }
        else {
            print("Error loading sound file")
        }
    }
    
    func pauseBGMusic() {

        if musPlayer != nil && musPlayer!.isPlaying {
            musPlayer!.pause()
        }
    }
    
    func stopBGMusic() {

        if musPlayer != nil {
            musPlayer!.stop()
        }
    }
    
    var scateCaptured = false
    
    func initCanvas(size: CGSize) {
        let backGround = SKSpriteNode(color: UIColor.clear, size: size)
        
        backGround.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        backGround.texture = SKTexture(imageNamed: "bg2")//"background")
        
        self.addChild(backGround)
        
        self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        
        let bBorder = SKPhysicsBody(edgeLoopFrom: CGRect(origin: CGPoint(x: 0, y: 0), size: self.frame.size))
        
        self.physicsBody = bBorder
        self.physicsBody?.friction = 0
        
        let hollowRect = CGRect(x: self.frame.origin.x, y: self.frame.origin.y, width: self.frame.width, height: 1)
        let hollow = SKNode()
        
        hollow.name = Board.hollowCategory.name()
        hollow.physicsBody = SKPhysicsBody(edgeLoopFrom: hollowRect)
        hollow.physicsBody?.categoryBitMask = Board.hollowCategory.categoryBit()
        
        self.addChild(hollow)
    }
    
    func initBall(size: CGSize) {
                
        let ball = SKSpriteNode(color: UIColor.black, size: size)
        
        ball.name = Board.ballCategory.name()
        ball.position = CGPoint(x: self.frame.width / 2 - self.frame.width / 10, y: self.frame.height / 4)
        print(self.frame.size)
        ball.physicsBody = SKPhysicsBody(circleOfRadius: size.width)
        ball.texture = SKTexture(imageNamed: "ball")
        ball.physicsBody?.friction = 0
        ball.physicsBody?.restitution = 1
        ball.physicsBody?.linearDamping = 0
        ball.physicsBody?.allowsRotation = false
        ball.physicsBody?.categoryBitMask = Board.ballCategory.categoryBit()
        ball.physicsBody?.contactTestBitMask = Board.hollowCategory.categoryBit() | Board.brickCategory.categoryBit()
    
        self.addChild(ball)
    }
    
    func goAhead(vector: CGVector) {

        let ball = self.childNode(withName: SubjectCategory.ball.name())

        ball?.physicsBody?.velocity = CGVector(dx: 0, dy: 0)
        ball!.physicsBody?.applyImpulse(vector)
    }
    
    func initScate(size: CGSize) {
        let scate = SKSpriteNode(color: UIColor.brown, size: size)
        
        scate.name = Board.scateCategory.name()
        scate.position = CGPoint(x: self.frame.width / 2, y: 40)
        scate.physicsBody = SKPhysicsBody(rectangleOf: size)
        scate.physicsBody?.friction = 0
        scate.physicsBody?.restitution = 0.1
        scate.physicsBody?.isDynamic = false
        scate.physicsBody?.categoryBitMask = Board.scateCategory.categoryBit()
        
        self.addChild(scate)
    }
    
    func initBricks() {
        
        for i in 0..<rowsCount {
            
            for j in 0...(bricksInRow - 1) {
                
                let brick = SKSpriteNode(color: UIColor.darkGray, size: CGSize(width: CGFloat(brickWidth), height: CGFloat(brickHeight)))
                
                brick.name = Board.brickCategory.name()
                brick.position = CGPoint(x: CGFloat(offsetX + Float(j) * (brickWidth + spaceH) + brickWidth / 2),
                                         y: CGFloat(Float(self.frame.height) - Float(i) * (brickHeight + spaceV) - offsetY))
                brick.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: CGFloat(brickWidth), height: CGFloat(brickHeight)))
                brick.physicsBody?.friction = 0
                brick.physicsBody?.allowsRotation = false
                brick.physicsBody?.categoryBitMask = Board.brickCategory.categoryBit()
                brick.physicsBody?.isDynamic = false
                
                self.addChild(brick)
            }
        }
    }

    func setPosition(_ node: SKNode, _ pos: CGPoint) {
        node.position = pos
    }
    
    func showButtons() {
        
        let label = SKLabelNode(fontNamed: "Avenir-Black")
        let label2 = SKLabelNode(fontNamed: "Avenir-Black")
        let buttonWidth = CGFloat(self.frame.width / 8.0 - self.frame.width / 40.0)
        let settingsButton = SKSpriteNode(color: UIColor.blue, size: CGSize(width: buttonWidth, height: 20.0))
        settingsButton.name = Board.sButtonCategory.name()
        
        self.addChild(settingsButton)
        self.addChild(label)
        
        setPosition(settingsButton,
        CGPoint(x: self.frame.width - buttonWidth,
                y: CGFloat(Float(self.frame.height) - Float(rowsCount) * (brickHeight + spaceV) - 
                offsetY - Float(settingsButton.frame.height))))
        print(settingsButton.position)
        settingsButton.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: buttonWidth, height: 20.0))
        settingsButton.texture = SKTexture(imageNamed: "background")
        settingsButton.physicsBody?.friction = 0
        settingsButton.physicsBody?.isDynamic = false
        settingsButton.physicsBody?.categoryBitMask = Board.sButtonCategory.categoryBit()
        label.fontSize = 12
        label.fontColor = UIColor.darkText
        label.position = CGPoint(x: settingsButton.position.x,
                                 y: settingsButton.position.y - settingsButton.size.height * 1.2)
        label.text = ("settings")
        
        let pauseButton = SKSpriteNode(color: UIColor.blue, size: CGSize(width: buttonWidth, height: 20.0))
        
        pauseButton.name = Board.pButtonCategory.name()
        
        self.addChild(pauseButton)
        self.addChild(label2)
        
        setPosition(pauseButton,
            CGPoint(x: buttonWidth,
                    y: CGFloat(Float(self.frame.height) - Float(rowsCount) * (brickHeight + spaceV) - 
                    offsetY - Float(pauseButton.frame.height))))
        print(pauseButton.position)
        pauseButton.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: buttonWidth, height: 20.0))
        pauseButton.texture = SKTexture(imageNamed: "background")
        pauseButton.physicsBody?.friction = 0
        pauseButton.physicsBody?.isDynamic = false
        pauseButton.physicsBody?.categoryBitMask = Board.pButtonCategory.categoryBit()
        label2.fontSize = 12
        label2.fontColor = UIColor.darkText
        label2.position = CGPoint(x: pauseButton.position.x,
                                  y: pauseButton.position.y - pauseButton.frame.height * 1.2)
        label2.text = ("pause")
    }
    
    lazy var gameVC: GameViewController? = nil

    func removeBricks() {
        for o in self.children {
            let node = o as SKNode
            
            if node.name == SubjectCategory.brick.name() {
                node.removeFromParent()
            }
        }
    }
    
    func reDrawBoard() {
        removeBricks()
        initBricks()
        
        let ball = self.childNode(withName: SubjectCategory.ball.name())
        let scate = self.childNode(withName: SubjectCategory.scate.name())    
        let settingsButton = self.childNode(withName: SubjectCategory.sButton.name())
        let pauseButton = self.childNode(withName: SubjectCategory.pButton.name())
        let label = SKLabelNode(fontNamed: "Avenir-Black")
        let label2 = SKLabelNode(fontNamed: "Avenir-Black")
        let buttonWidth = CGFloat(self.frame.width / 8.0 - self.frame.width / 30.0)
        
        setPosition(settingsButton!,
        CGPoint(x: self.frame.width - buttonWidth,
                y: CGFloat(Float(self.frame.height) - Float(rowsCount) * (brickHeight + spaceV) - 
                offsetY - Float(settingsButton!.frame.height))))
        label.fontSize = 12
        label.fontColor = UIColor.darkText
        label.position = CGPoint(x: settingsButton!.position.x,
                                 y: settingsButton!.position.y - settingsButton!.frame.height * 1.2)
        
        label.text = ("settings")
        
        setPosition(pauseButton!, 
            CGPoint(x: buttonWidth,
                    y: CGFloat(Float(self.frame.height) - Float(rowsCount) * (brickHeight + spaceV) -
                    offsetY - Float(pauseButton!.frame.height))))
        label2.fontSize = 12
        label2.fontColor = UIColor.darkText
        label2.position = CGPoint(x: pauseButton!.position.x,
                                  y: pauseButton!.position.y - pauseButton!.frame.height * 1.2)
        label2.text = ("pause")

        setPosition(ball!, CGPoint(x: self.frame.width / 2 - self.frame.width / 10, y: self.frame.height / 4))
        setPosition(scate!, CGPoint(x: self.frame.width / 2, y: 40))
        goAhead(vector: CGVector(dx: 3, dy: -3))
        playBGMusic()
    }
    
    override init(size: CGSize) {
        spaceH = 10.0
        spaceV = 10.0
        rowsCount = 3
        bricksInRow = 6
        offsetX = 20.0
        offsetY = 20.0
        
        super.init(size: size)
        self.physicsWorld.contactDelegate = self

        initCanvas(size: size)
        initBall(size: CGSize(width: self.frame.width / 40, height: self.frame.width / 40))
        initScate(size: CGSize(width: self.frame.width / 5, height: self.frame.height / 12))
        initBricks()
        showButtons()
        
        goAhead(vector: CGVector(dx: 3, dy: -3))
        playBGMusic()
    }
    
    required init?(coder aDecoder: NSCoder) {
        spaceH = 5.0
        spaceV = 5.0
        rowsCount = 3
        bricksInRow = 8
        offsetX = 10.0
        offsetY = 10.0
        
        super.init(coder: aDecoder)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first
        let touchLocation = touch?.location(in: self)
        let body: SKPhysicsBody? = self.physicsWorld.body(at: touchLocation!)
        
        if body?.node?.name == SubjectCategory.scate.name() {
            scateCaptured = true
        }
        
        if body?.node?.name == SubjectCategory.pButton.name() {
            if !self.view!.isPaused {
                self.view!.isPaused = true
                pauseBGMusic()
            }
            else {
                self.view!.isPaused = false
                playBGMusic()
            }
        }
        
        if body?.node?.name == SubjectCategory.sButton.name() {
            self.view!.isPaused = true
            pauseBGMusic()
            gameVC!.showSetup()
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if scateCaptured {
            let touch = touches.first
            let touchLocation = touch?.location(in: self)
            let prevTouchLocation = touch?.previousLocation(in: self)
            let scate = self.childNode(withName: SubjectCategory.scate.name())
            var newX = scate!.position.x + (touchLocation!.x - prevTouchLocation!.x)
           
            newX = max(newX, scate!.frame.width / 2)
            newX = min(newX, self.frame.width - scate!.frame.width / 2)
            scate?.position = CGPoint(x: newX, y: scate!.position.y)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        scateCaptured = false
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        
        var bodyOne = SKPhysicsBody()
        var bodyTwo = SKPhysicsBody()
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            bodyOne = contact.bodyA
            bodyTwo = contact.bodyB
        }
        else {
            bodyOne = contact.bodyB
            bodyTwo = contact.bodyA
        }
        
        if bodyOne.categoryBitMask == SubjectCategory.ball.categoryBit() &&
           bodyTwo.categoryBitMask == SubjectCategory.hollow.categoryBit() {
            stopBGMusic()
            musPlayer = nil
            gameVC!.showOver(won: false)
            return
        }
        
        if bodyOne.categoryBitMask == SubjectCategory.ball.categoryBit() &&
            bodyTwo.categoryBitMask == SubjectCategory.brick.categoryBit() {
            bodyTwo.node?.removeFromParent()
        }
        
        if gameWon() {
            gameVC!.showOver(won: true)
        }
    }
    
    func gameWon() -> Bool {
        var bricksCount = 0
        
        for o in self.children {
            let node = o as SKNode
            
            if node.name == SubjectCategory.brick.name() {
                bricksCount += 1
            }
        }
        
        return bricksCount == 0
    }
    
}
