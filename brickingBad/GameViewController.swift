//
//  GameViewController.swift
//  brickingBad
//
//  Created by Nunusick on 12/2/18.
//  Copyright © 2018 s. All rights reserved.
//


import UIKit
import SpriteKit

class GameViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var spaceHTF: UITextField!
    @IBOutlet weak var spaceVTF: UITextField!
    @IBOutlet weak var rowsCount: UITextField!
    @IBOutlet weak var bricksInRow: UITextField!
    @IBOutlet weak var offsetYTF: UITextField!
    @IBOutlet weak var offsetXTF: UITextField!
    @IBOutlet weak var statusLabel: UILabel!

    @IBOutlet weak var setupView: UIView!
    
    @IBAction func applySettings(_ sender: UIButton) {
        setupView.removeFromSuperview()
        view.setNeedsDisplay()
        showBoard(redraw: true)
    }
    
    lazy var gameBoard: Board? = nil
    lazy var skView: SKView? = nil
    lazy var gameOver: GameOver? = nil
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let textEntered = textField.text
        
        textField.resignFirstResponder()
        
        switch textField.placeholder {
        case "spaceH":
            
            if let spaceH = Float(textEntered!) {
                gameBoard!.spaceH = spaceH
                statusLabel.text = "Successfully set new value for spaceH - \(gameBoard!.spaceH)"
            }
            else {
                statusLabel.text = "Incorrect value entered - will use previous."
            }
            spaceVTF.becomeFirstResponder()
            
        case "spaceV":
            
            if let spaceV = Float(textEntered!) {
                gameBoard!.spaceV = spaceV
                statusLabel.text = "Successfully set new value for spaceV - \(gameBoard!.spaceV)"
            }
            else {
                statusLabel.text = "Incorrect value entered - will use previous."
            }
            rowsCount.becomeFirstResponder()
            
        case "rowsCount":
            
            if let rowsCount = Int(textEntered!) {
                gameBoard!.rowsCount = rowsCount
                statusLabel.text = "Successfully set new value for rowsCount - \(gameBoard!.rowsCount)"
            }
            else {
                statusLabel.text = "Incorrect value entered - will use previous."
            }
            bricksInRow.becomeFirstResponder()
            
        case "bricksInRow":
            
            if let bricksInRow = Int(textEntered!) {
                gameBoard!.bricksInRow = bricksInRow
                statusLabel.text = "Successfully set new value for bricksInRow - \(gameBoard!.bricksInRow)"
            }
            else {
                statusLabel.text = "Incorrect value entered - will use previous or default."
            }
            offsetXTF.becomeFirstResponder()
            
        case "offsetX":
            
            if let offsetX = Float(textEntered!) {
                gameBoard!.offsetX = offsetX
                statusLabel.text = "Successfully set new value for offsetX - \(gameBoard!.offsetX)"
            }
            else {
                statusLabel.text = "Incorrect value entered - will use previous."
            }
            offsetYTF.becomeFirstResponder()
            
        case "offsetY":
            if let offsetY = Float(textEntered!) {
                gameBoard!.offsetY = offsetY
                statusLabel.text = "Successfully set new value for offsetY - \(gameBoard!.offsetY)"
            }
            else {
                statusLabel.text = "Incorrect value entered - will use previous."
            }
            spaceHTF.becomeFirstResponder()
            
        default: break
        }
        
        return true
    }
    
    func showSetup() {
        spaceHTF.text = String("\(gameBoard!.spaceH)")
        spaceVTF.text = String("\(gameBoard!.spaceV)")
        rowsCount.text = String("\(gameBoard!.rowsCount)")
        bricksInRow.text = String("\(gameBoard!.bricksInRow)")
        offsetXTF.text = String("\(gameBoard!.offsetX)")
        offsetYTF.text = String("\(gameBoard!.offsetY)")
        view.addSubview(setupView)
        view.setNeedsDisplay()
    }
    
    func showOver(won: Bool) {
        skView?.scene?.removeFromParent()
        
        if gameOver == nil {
            gameOver = GameOver(size: view.frame.size, won: won)
            gameOver!.gameVC = self
            gameOver!.scaleMode = .resizeFill
        }
        else {
            gameOver!.won = won
        }
        
        skView?.showsFPS = true
        skView?.showsNodeCount = true
        skView?.ignoresSiblingOrder = false
        skView?.presentScene(gameOver!)
        view.setNeedsDisplay()
    }
    
    func showBoard(redraw: Bool = false) {
        skView?.scene?.removeFromParent()
        if gameBoard == nil {
            gameBoard = Board(size: view.frame.size)
            gameBoard!.gameVC = self
            gameBoard!.scaleMode = .resizeFill
        }
        else {
            if redraw {
                gameBoard!.reDrawBoard()
            }
        }
        
        skView?.showsFPS = true
        skView?.showsNodeCount = true
        skView?.ignoresSiblingOrder = false
        skView?.presentScene(gameBoard!)
        view.setNeedsDisplay()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        skView = view as? SKView
        
        if skView?.scene == nil {
            showBoard()
        }
    }
}
